
package com.codeoftheweb.salvo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class SalvoController {


    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private ShipRepository shipRepository;

    @Autowired
    private SalvoRepository salvoRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public List<Object> getGames() {
        return gameRepository
                .findAll()
                .stream()
                .map(game -> gameDTO(game))
                .collect(Collectors.toList());
    }

    public Map<String, Object> gameDTO(Game game) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", game.getId());                                          //get and put game id
        dto.put("DateCreated", game.getCreationDate());                      //get and put game creation date
        dto.put("gamePlayers", getGamePlayersList(game.getGamePlayers()));    //get gameplayers info
        dto.put("scores", getScoresList(game.getScores()));
        return dto;
    }


    //changing the controller for the /game uri to generate json for task 1 of part 2
    @RequestMapping("/games")
    public Map<String, Object> makeLoggedPlayer(Authentication authentication) {
        Map<String, Object> dto = new LinkedHashMap<>();
        if (isGuest(authentication))
            dto.put("player", "Guest");
        else
            dto.put("player", loggedPlayerDTO(getAuthPlayer(authentication)));
        dto.put("games", getGames());

        return dto;
    }

    public Map<String, Object> loggedPlayerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", player.getId());
        dto.put("username", player.getUserName());
        return dto;
    }

    private Player getAuthPlayer(Authentication authentication) {
        return playerRepository.findByUserName(authentication.getName());
    }

    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }


    public List<Object> getGamePlayersList(Set<GamePlayer> gamePlayers) {
        return gamePlayers
                .stream()
                .map(gamePlayer -> gamePlayerDTO(gamePlayer))
                .collect(Collectors.toList());

    }

    public Map<String, Object> gamePlayerDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", gamePlayer.getId());
        dto.put("DateCreated", gamePlayer.getJoinDate());        //get and put gameplayer creation date
        dto.put("player", playerDTO(gamePlayer.getPlayer()));  //get player info
        return dto;
    }

    @RequestMapping("/leaderBoard")
    public List<Map<String, Object>> makeLeaderBoard() {
        return playerRepository
                .findAll()
                .stream()
                .map(player -> playerDTO(player))
                .collect(Collectors.toList());
    }

    public Map<String, Object> playerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", player.getId());
        dto.put("username", player.getUserName());
        dto.put("scores", makeScoresList(player));
        return dto;
    }



    @RequestMapping("/game_view/{id}")
    public ResponseEntity<Object> cheat(@PathVariable long id, Authentication authentication) {
        Player player = getLoggedPlayer(authentication);
        GamePlayer gamePlayer = gamePlayerRepository.findById(id).orElse(null);
        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "Forbidden"), HttpStatus.FORBIDDEN);
        }

        if (player.getId() != gamePlayer.getPlayer().getId()) {
            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
        //return gameViewDTO(gamePlayerRepository.findById(id).get());


        //return dto;
        return new ResponseEntity<>(gameViewDTO(gamePlayerRepository.findById(id).get()), HttpStatus.OK);
    }

    Player getLoggedPlayer(Authentication authentication) {
        return playerRepository.findByUserName(authentication.getName());
    }

    public Map<String, Object> gameViewDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<>();
        List<GamePlayer> gamePlayers = gamePlayerRepository.findAll().stream().filter( gp -> gp.getGame().getId() == gamePlayer.getGame().getId()).collect(Collectors.toList());
        GamePlayer self = gamePlayers.stream().filter(gp -> gp.getId() == gamePlayer.getId()).findFirst().orElse(null);
        GamePlayer opponent = gamePlayers.stream().filter(gp -> gp.getId() != gamePlayer.getId()).findFirst().orElse(null);

        dto.put("id", gamePlayer.getGame().getId());
        dto.put("DateCreated", gamePlayer.getGame().getCreationDate());
        dto.put("gamePlayers", getGamePlayersList(gamePlayer.getGame().getGamePlayers()));
        dto.put("ships", getShipList(gamePlayer.getShips()));
        dto.put("salvoes", makeSalvoList(gamePlayer.getGame()));
        dto.put("hits", makeHitsDTO(self, opponent));
        return dto; //AYUDA
    }


    public List<Map<String, Object>> getShipList(Set<Ship> ships) {
        return ships
                .stream()
                .map(ship -> shipDTO(ship))
                .collect(Collectors.toList());
    }

    public Map<String, Object> shipDTO(Ship ship) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("shipType", ship.getShipType());
        dto.put("locations", ship.getLocations());

        return dto;
    }


    //create de salvo dto
    public List<Map<String, Object>> makeSalvoList(Game game) {
        List<Map<String, Object>> myList = new ArrayList<>();
        game.getGamePlayers().forEach(gamePlayer -> myList.addAll(getSalvoList(gamePlayer.getSalvoes())));
        return myList;
    }

    //create the salvo list for the json
    public List<Map<String, Object>> getSalvoList(List<Salvo> salvoes) {
        return salvoes
                .stream()
                .map(salvo -> salvoDTO(salvo))
                .collect(Collectors.toList());
    }

    public Map<String, Object> salvoDTO(Salvo salvo) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("locations", salvo.getLocations());
        dto.put("turn", salvo.getTurn());
        dto.put("player", salvo.getGamePlayer().getPlayer().getId());
        return dto;
    }


    public Map<String, Object> makeScoresList(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("name", player.getUserName());
        dto.put("total", player.getScore(player));
        dto.put("won", player.getWins(player.getScores()));
        dto.put("lost", player.getLoses(player.getScores()));
        dto.put("tied", player.getDraws(player.getScores()));
        return dto;
    }

    public List<Object> getScoresList(List<Score> scores) {
        return scores
                .stream()
                .map(score -> scoreDTO(score))
                .collect(Collectors.toList());
    }

    public Map<String, Object> scoreDTO(Score score) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("playerId", score.getPlayer().getId());
        dto.put("score", score.getScore());
        dto.put("finishDate", score.getFinishDate());
        return dto;
    }

    public Map<String, Object> makeScoreDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("playerId", gamePlayer.getPlayer().getId());
        if (gamePlayer.getScore() != null)
            dto.put("score", gamePlayer.getScore().getScore());

        return dto;
    }


    @RequestMapping(path = "/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createPlayer(@RequestParam String username, @RequestParam String password) {
        if (username.isEmpty() || password.isEmpty()) {
            return new ResponseEntity<>(makeMap("error", "No name"), HttpStatus.FORBIDDEN);
        }
        Player player = playerRepository.findByUserName(username);
        if (player != null) {
            return new ResponseEntity<>(makeMap("error", "Username already exists"), HttpStatus.CONFLICT);
        }
        Player newPlayer = playerRepository.save(new Player(username, passwordEncoder.encode(password)));
        return new ResponseEntity<>(makeMap("id", newPlayer.getId()), HttpStatus.CREATED);
    }

    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    @RequestMapping(path = "/games", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createGame( Authentication authentication) {
        Player player = getLoggedPlayer(authentication);
        GamePlayer gamePlayer = gamePlayerRepository.findById(player.getId()).orElse(null);
        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "You need to be logged in"), HttpStatus.UNAUTHORIZED);
        }
        Game newGame = gameRepository.save(new Game(new Date()));

        GamePlayer newGamePlayer = gamePlayerRepository.save(new GamePlayer(newGame, player));

        return new ResponseEntity<>(makeMap("id", newGamePlayer.getId()), HttpStatus.CREATED);
        }



    @RequestMapping(path ="/game/{id}/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> joinGame(Authentication authentication, @PathVariable long id) {

            Game game = gameRepository.findById(id).orElse(null);

            if (isGuest(authentication)) {
                return new ResponseEntity<>(makeMap("error", "You need to be logged in"), HttpStatus.UNAUTHORIZED);
            }

            if (game == null) {
                return new ResponseEntity<>(makeMap("error", "No such game"), HttpStatus.FORBIDDEN);
            }

            long playersCount = gamePlayerRepository.findAll().stream().filter(gamePlayer -> gamePlayer.getGame().getId() == id).count();
            if(playersCount == 2){
                return new ResponseEntity<>(makeMap("error", "Game is full"), HttpStatus.FORBIDDEN);
            }
            GamePlayer gp = new GamePlayer(game, playerRepository.findByUserName(authentication.getName()));
            gamePlayerRepository.saveAndFlush(gp);

            GamePlayer gp1 = game.getGamePlayers().stream().filter(gamePlayer -> gamePlayer.getId() != gp.getId()).findFirst().orElse(null);
            gamePlayerRepository.saveAndFlush(gp1);

            return new ResponseEntity<>( makeMap("id", gp.getId()) , HttpStatus.CREATED);
        }

        @RequestMapping(path="games/players/{gamePlayerId}/ships", method = RequestMethod.POST)
        public ResponseEntity<Map<String, Object>> addShips(@PathVariable long gamePlayerId, @RequestBody Set<Ship> ships, Authentication authentication) {

            if(isGuest(authentication)) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

            Player player = getLoggedPlayer(authentication);

            GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).orElse(null);

            if(gamePlayer == null) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

            if(player.getId() != gamePlayer.getPlayer().getId()) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);



            if(gamePlayer.getShips().size() >= 5) return new ResponseEntity<>( HttpStatus.FORBIDDEN);

            for (Ship ship : ships){
                ship.setGamePlayer(gamePlayer);
                shipRepository.save(ship);
            }

            return new ResponseEntity<>( makeMap("OK", "Ships placed"), HttpStatus.OK);

        }


    @RequestMapping(path="games/players/{gamePlayerId}/salvoes", method = RequestMethod.POST)
    public ResponseEntity <Map<String, Object>> addSalvoes(@PathVariable long gamePlayerId, @RequestBody Salvo salvo, Authentication authentication) {

        if(isGuest(authentication)) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        Player player = getLoggedPlayer(authentication);

        GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).orElse(null);

        if(gamePlayer == null) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        if(player.getId() != gamePlayer.getPlayer().getId()) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        if (!gamePlayer.isTurnLoaded(salvo.getTurn())) {
            salvo.setGamePlayer(gamePlayer);
            salvoRepository.save(salvo);
            return new ResponseEntity<>(makeMap("OK", "Salvo placed"), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(makeMap("error", "Player already placed salvoes in this turn"), HttpStatus.FORBIDDEN);
        }

    }



    private List<Map> getHits(GamePlayer gamePlayer, GamePlayer opponentGameplayer) { //le paso dos gameplayers
        List<Map> hits = new ArrayList<>();
        Integer carrierDamage = 0;
        Integer battleshipDamage = 0;
        Integer submarineDamage = 0;
        Integer destroyerDamage = 0;
        Integer patrolboatDamage = 0; //declaro variables
        List <String> carrierLocation = new ArrayList<>();
        List <String> battleshipLocation = new ArrayList<>();
        List <String> submarineLocation = new ArrayList<>();
        List <String> destroyerLocation = new ArrayList<>();
        List <String> patrolboatLocation = new ArrayList<>(); //creo una nueva lista con las ubicaciones de cada barco
        gamePlayer.getShips().forEach(ship -> {
            switch (ship.getShipType()) {
                case "Carrier":
                    carrierLocation.addAll(ship.getLocations());
                    break;
                case "Battleship":
                    battleshipLocation.addAll(ship.getLocations());
                    break;
                case "Submarine":
                    submarineLocation.addAll(ship.getLocations());
                    break;
                case "Destroyer":
                    destroyerLocation.addAll(ship.getLocations());
                    break;
                case "Patrol Boat":
                    patrolboatLocation.addAll(ship.getLocations());    //paso las ubicaciones de gameplayer a las nuevas listas
                    break;
            }
        });
        for (Salvo salvo : opponentGameplayer.getSalvoes()) {  //obtener los salvos del oponente
            Integer carrierHitsInTurn = 0;
            Integer battleshipHitsInTurn = 0;
            Integer submarineHitsInTurn = 0;
            Integer destroyerHitsInTurn = 0;
            Integer patrolboatHitsInTurn = 0; //declaro variables de los hits acertados
            Integer missedShots = salvo.getLocations().size(); //para ir descontando los tiros errados
            Map<String, Object> hitsMapPerTurn = new LinkedHashMap<>();  //mapa de los hits
            Map<String, Object> damagesPerTurn = new LinkedHashMap<>();  //mapa del daño que le hace al gp?
            List<String> salvoLocationsList = new ArrayList<>(); //
            List<String> hitCellsList = new ArrayList<>();  //acumula las celdas pegadas
            salvoLocationsList.addAll(salvo.getLocations());
            for (String salvoShot : salvoLocationsList) {
                if (carrierLocation.contains(salvoShot)) {
                    carrierDamage++;
                    carrierHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (battleshipLocation.contains(salvoShot)) {
                    battleshipDamage++;
                    battleshipHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (submarineLocation.contains(salvoShot)) {
                    submarineDamage++;
                    submarineHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (destroyerLocation.contains(salvoShot)) {
                    destroyerDamage++;
                    destroyerHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (patrolboatLocation.contains(salvoShot)) {
                    patrolboatDamage++;
                    patrolboatHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
            }
            damagesPerTurn.put("carrierHits", carrierHitsInTurn);
            damagesPerTurn.put("battleshipHits", battleshipHitsInTurn);
            damagesPerTurn.put("submarineHits", submarineHitsInTurn);
            damagesPerTurn.put("destroyerHits", destroyerHitsInTurn);
            damagesPerTurn.put("patrolboatHits", patrolboatHitsInTurn);
            damagesPerTurn.put("Carrier", carrierDamage);
            damagesPerTurn.put("Battleship", battleshipDamage);
            damagesPerTurn.put("Submarine", submarineDamage);
            damagesPerTurn.put("Destroyer", destroyerDamage);
            damagesPerTurn.put("Patrol Boat", patrolboatDamage);
            hitsMapPerTurn.put("turn", salvo.getTurn());
            hitsMapPerTurn.put("hitLocations", hitCellsList);
            hitsMapPerTurn.put("damages", damagesPerTurn);
            hitsMapPerTurn.put("missed", missedShots);
            hits.add(hitsMapPerTurn);
        }
        return hits;
    }

    //mapeo los datos de un turno, tanto del jugador y de su oponente
    private Map<String, Object> makeHitsDTO(GamePlayer selfGP, GamePlayer opponentGP){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("self", getHits(selfGP, opponentGP));
        dto.put("opponent", getHits(opponentGP, selfGP));
        return dto;
    }

    }








