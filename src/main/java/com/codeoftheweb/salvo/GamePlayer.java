package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Entity
public class GamePlayer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    @ManyToOne(fetch = FetchType.EAGER) //Como si fuera un select generico
    @JoinColumn(name="player_id")
    private Player player;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="game_id")
    private Game game;

    @OneToMany(mappedBy="gamePlayer", fetch=FetchType.EAGER)
    private Set<Ship> ships;   //tambien puede ser Set<Ship> ships = new HashSet<>();

    @OneToMany(mappedBy="gamePlayer")
    private  List<Salvo> salvoes;

    private Date joinDate;



    //constructores
    public GamePlayer(){
    }

    public GamePlayer(Game game, Player player) {
        this.setGame(game);
        this.setPlayer(player);
        this.joinDate = new Date();
    }



    public void setGamePlayer (Player player){

    }

    public void add (GamePlayer gameplayer){

    }

    public boolean isTurnLoaded (int turn) {
        Salvo s = this.getSalvoes()
                .stream()
                .filter(salvo -> salvo.getTurn() == turn)
                .findFirst()
                .orElse(null);

        if (s == null) {
            return false;
        } else {
            return true;
        }
    }

/*
    public void addShip(){

    }
*/





    //getters y setters
    @JsonIgnore
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
    @JsonIgnore
    public Game getGame() {
        return game;
    }

    public void setGame(Game game){
        this.game = game;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinGameDate) {
        this.joinDate = joinGameDate;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonIgnore
    public Set<Ship> getShips() {
        return ships;
    }

    public void setShips(Set<Ship> ships) {
        this.ships = ships;
    }


    public List<Salvo> getSalvoes() {
        return salvoes;
    }

    public void setSalvos(List<Salvo> salvoes) {
        this.salvoes = salvoes;
    }

    public Score getScore() {
        return getPlayer().getScore(getGame());
    }
}