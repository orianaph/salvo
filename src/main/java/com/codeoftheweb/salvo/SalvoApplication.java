package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class SalvoApplication {



        public static void main(String[] args) {
                SpringApplication.run(SalvoApplication.class, args);
        }


        @Bean
        public CommandLineRunner initData(PlayerRepository playerRepository,
                                          GameRepository gameRepository,
                                          GamePlayerRepository gamePlayerRepository,
                                          ShipRepository shipRepository,
                                          SalvoRepository salvoRepository,
                                          ScoreRepository scoreRepository) {
                return (args) -> {



                        Date d1 = new Date();
                        Date d2 = Date.from(d1.toInstant().plusSeconds(3600));
                        Date d3 = Date.from(d2.toInstant().plusSeconds(3600));
                        Date d4 = Date.from(d3.toInstant().plusSeconds(3600));
                        Date d5 = Date.from(d4.toInstant().plusSeconds(3600));
                        Date d6 = Date.from(d5.toInstant().plusSeconds(3600));
                        Date d7 = Date.from(d6.toInstant().plusSeconds(3600));
                        Date d8 = Date.from(d7.toInstant().plusSeconds(3600));


                        Game g1 = new Game(d1);
                        Game g2 = new Game(d2);
                        Game g3 = new Game(d3);
                        Game g4 = new Game(d4);
                        Game g5 = new Game(d5);
                        Game g6 = new Game(d6);
                        Game g7 = new Game(d7);
                        Game g8 = new Game(d8);


                        gameRepository.save(g1);
                        gameRepository.save(g2);
                        gameRepository.save(g3);
                        gameRepository.save(g4);
                        gameRepository.save(g5);
                        gameRepository.save(g6);
                        gameRepository.save(g7);
                        gameRepository.save(g8);



                        Player p1 = new Player("j.bauer@ctu.gov", passwordEncoder().encode("24"));
                        Player p2 = new Player( "c.obrian@ctu.gov", passwordEncoder().encode("42"));
                        Player p3 = new Player("kim_bauer@gmail.com", passwordEncoder().encode("kb"));
                        Player p4 = new Player( "t.almeida@ctu.gov", passwordEncoder().encode("mole"));


                        playerRepository.save(p1);
                        playerRepository.save(p2);
                        playerRepository.save(p3);
                        playerRepository.save(p4);




                        GamePlayer gp1 = new GamePlayer (g1, p1);
                        GamePlayer gp2 = new GamePlayer (g1, p2);
                        GamePlayer gp3 = new GamePlayer (g2, p1);
                        GamePlayer gp4 = new GamePlayer (g2, p2);
                        GamePlayer gp5 = new GamePlayer (g3, p2);
                        GamePlayer gp6 = new GamePlayer (g3, p4);
                        GamePlayer gp7 = new GamePlayer (g4, p2);
                        GamePlayer gp8 = new GamePlayer (g4, p3);
                        GamePlayer gp9 = new GamePlayer (g5, p4);
                        GamePlayer gp10 = new GamePlayer (g5, p1);
                        GamePlayer gp11 = new GamePlayer (g6, p3);
                        //GamePlayer gp12 = new GamePlayer (g6, p2);
                        GamePlayer gp13 = new GamePlayer (g7, p4);
                        //GamePlayer gp14 = new GamePlayer (g7, p2);
                        GamePlayer gp15 = new GamePlayer (g8, p3);
                        GamePlayer gp16 = new GamePlayer (g8, p4);

                        gamePlayerRepository.save(gp1);
                        gamePlayerRepository.save(gp2);
                        gamePlayerRepository.save(gp3);
                        gamePlayerRepository.save(gp4);
                        gamePlayerRepository.save(gp5);
                        gamePlayerRepository.save(gp6);
                        gamePlayerRepository.save(gp7);
                        gamePlayerRepository.save(gp8);
                        gamePlayerRepository.save(gp9);
                        gamePlayerRepository.save(gp10);
                        gamePlayerRepository.save(gp11);
                        //gamePlayerRepository.save(gp12);
                        gamePlayerRepository.save(gp13);
                        //gamePlayerRepository.save(gp14);
                        gamePlayerRepository.save(gp15);
                        gamePlayerRepository.save(gp16);

                        List<String> locationShip1 = new ArrayList<>();
                        locationShip1.add("H2");
                        locationShip1.add("H3");
                        locationShip1.add("H4");
                        List<String> locationShip2 = new ArrayList<>();
                        locationShip2.add("B4");
                        locationShip2.add("B5");
                        List<String> locationShip3 = new ArrayList<>();
                        locationShip3.add("B5");
                        locationShip3.add("C5");
                        locationShip3.add("D5");
                        List<String> locationShip4 = new ArrayList<>();
                        locationShip4.add("F1");
                        locationShip4.add("F2");
                        List<String> locationShip5 = new ArrayList<>();
                        locationShip5.add("C6");
                        locationShip5.add("C7");
                        List<String> locationShip6 = new ArrayList<>();
                        locationShip6.add("A2");
                        locationShip6.add("A3");
                        locationShip6.add("A4");
                        List<String> locationShip7 = new ArrayList<>();
                        locationShip7.add("G6");
                        locationShip7.add("H6");
                        List<String> locationSalvo1 = new ArrayList<>();
                        locationSalvo1.add("B5");
                        locationSalvo1.add("C5");
                        locationSalvo1.add("F1");
                        List<String> locationSalvo2 = new ArrayList<>();
                        locationSalvo2.add("B4");
                        locationSalvo2.add("B5");
                        locationSalvo2.add("B6");
                        List<String> locationSalvo3 = new ArrayList<>();
                        locationSalvo3.add("F2");
                        locationSalvo3.add("D5");
                        List<String> locationSalvo4 = new ArrayList<>();
                        locationSalvo4.add("E1");
                        locationSalvo4.add("H3");
                        locationSalvo4.add("A2");
                        List<String> locationSalvo5 = new ArrayList<>();
                        locationSalvo5.add("A2");
                        locationSalvo5.add("A4");
                        locationSalvo5.add("G6");
                        List<String> locationSalvo6 = new ArrayList<>();
                        locationSalvo6.add("B5");
                        locationSalvo6.add("D5");
                        locationSalvo6.add("C7");
                        List<String> locationSalvo7 = new ArrayList<>();
                        locationSalvo7.add("A3");
                        locationSalvo7.add("H6");
                        List<String> locationSalvo8 = new ArrayList<>();
                        locationSalvo8.add("C5");
                        locationSalvo8.add("C6");
                        List<String> locationSalvo9 = new ArrayList<>();
                        locationSalvo9.add("G6");
                        locationSalvo9.add("H6");
                        locationSalvo9.add("A4");
                        List<String> locationSalvo10 = new ArrayList<>();
                        locationSalvo10.add("H1");
                        locationSalvo10.add("H2");
                        locationSalvo10.add("H3");
                        List<String> locationSalvo11 = new ArrayList<>();
                        locationSalvo11.add("A2");
                        locationSalvo11.add("A3");
                        locationSalvo11.add("B8");
                        List<String> locationSalvo12 = new ArrayList<>();
                        locationSalvo12.add("E1");
                        locationSalvo12.add("F2");
                        locationSalvo12.add("G3");
                        List<String> locationSalvo13 = new ArrayList<>();
                        locationSalvo13.add("A3");
                        locationSalvo13.add("A4");
                        locationSalvo13.add("F7");
                        List<String> locationSalvo14 = new ArrayList<>();
                        locationSalvo14.add("B5");
                        locationSalvo14.add("B6");
                        locationSalvo14.add("H1");
                        List<String> locationSalvo15 = new ArrayList<>();
                        locationSalvo15.add("A2");
                        locationSalvo15.add("G6");
                        locationSalvo15.add("H6");
                        List<String> locationSalvo16 = new ArrayList<>();
                        locationSalvo16.add("C5");
                        locationSalvo16.add("C7");
                        locationSalvo16.add("D5");

                        Ship ship1 = new Ship("Destroyer", gp1, locationShip1);
                        Ship ship2 = new Ship("Submarine", gp1, locationShip2);
                        Ship ship3 = new Ship("Destroyer", gp2, locationShip3);
                        Ship ship4 = new Ship("Patrol Boat", gp2, locationShip4);
                        Ship ship5 = new Ship("Destroyer", gp3, locationShip3);
                        Ship ship6 = new Ship("Patrol Boat", gp3, locationShip5);
                        Ship ship7 = new Ship("Submarine", gp4, locationShip6);
                        Ship ship8 = new Ship("Patrol Boat", gp4, locationShip7);
                        Ship ship9 = new Ship("Destroyer", gp5, locationShip3);
                        Ship ship10 = new Ship("Patrol Boat", gp5, locationShip5);
                        Ship ship11 = new Ship("Submarine", gp6, locationShip6);
                        Ship ship12 = new Ship("Patrol Boat", gp6, locationShip7);
                        Ship ship13 = new Ship("Destroyer", gp7, locationShip3);
                        Ship ship14 = new Ship("Patrol Boat", gp7, locationShip5);
                        Ship ship15 = new Ship("Submarine", gp8, locationShip6);
                        Ship ship16 = new Ship("Patrol Boat", gp8, locationShip7);

                        shipRepository.save(ship1);
                        shipRepository.save(ship2);
                        shipRepository.save(ship3);
                        shipRepository.save(ship4);
                        shipRepository.save(ship5);
                        shipRepository.save(ship6);
                        shipRepository.save(ship7);
                        shipRepository.save(ship8);
                        shipRepository.save(ship9);
                        shipRepository.save(ship10);
                        shipRepository.save(ship11);
                        shipRepository.save(ship12);
                        shipRepository.save(ship13);
                        shipRepository.save(ship14);
                        shipRepository.save(ship15);




                        Salvo salvo1 = new Salvo(gp1,1,locationSalvo1);
                        Salvo salvo2 = new Salvo(gp2,1,locationSalvo2);
                        shipRepository.save(ship16);
                        Salvo salvo3 = new Salvo(gp1,2,locationSalvo3);
                        Salvo salvo4 = new Salvo(gp2,2,locationSalvo4);
                        Salvo salvo5 = new Salvo(gp3,1,locationSalvo5);
                        Salvo salvo6 = new Salvo(gp4,1,locationSalvo6);
                        Salvo salvo7 = new Salvo(gp3,2,locationSalvo7);
                        Salvo salvo8 = new Salvo(gp4,2,locationSalvo8);
                        Salvo salvo9 = new Salvo(gp5,1,locationSalvo9);
                        Salvo salvo10 = new Salvo(gp6,1,locationSalvo10);
                        Salvo salvo11 = new Salvo(gp5,2,locationSalvo11);
                        Salvo salvo12 = new Salvo(gp6,2,locationSalvo12);
                        Salvo salvo13 = new Salvo(gp7,1,locationSalvo13);
                        Salvo salvo14 = new Salvo(gp8,1,locationSalvo14);
                        Salvo salvo15 = new Salvo(gp7,2,locationSalvo15);
                        Salvo salvo16 = new Salvo(gp8,2,locationSalvo15);


                        salvoRepository.save(salvo1);
                        salvoRepository.save(salvo2);
                        salvoRepository.save(salvo3);
                        salvoRepository.save(salvo4);
                        salvoRepository.save(salvo5);
                        salvoRepository.save(salvo6);
                        salvoRepository.save(salvo7);
                        salvoRepository.save(salvo8);
                        salvoRepository.save(salvo9);
                        salvoRepository.save(salvo10);
                        salvoRepository.save(salvo11);
                        salvoRepository.save(salvo12);
                        salvoRepository.save(salvo13);
                        salvoRepository.save(salvo14);
                        salvoRepository.save(salvo15);
                        salvoRepository.save(salvo16);

                        Score score1 = new Score(p1, g1,1.0, Date.from(d1.toInstant().plusSeconds(1800)));
                        Score score2 = new Score(p2, g1,0.0, Date.from(d1.toInstant().plusSeconds(1800)));
                        Score score3 = new Score(p1, g2,0.5, Date.from(d2.toInstant().plusSeconds(1800)));
                        Score score4 = new Score(p2, g2,0.5, Date.from(d2.toInstant().plusSeconds(1800)));
                        Score score5 = new Score(p2, g3,1.0, Date.from(d3.toInstant().plusSeconds(1800)));
                        Score score6 = new Score(p4, g3,0.0, Date.from(d3.toInstant().plusSeconds(1800)));
                        Score score7 = new Score(p2, g4,0.5, Date.from(d4.toInstant().plusSeconds(1800)));
                        Score score8 = new Score(p1, g4,0.5, Date.from(d4.toInstant().plusSeconds(1800)));

                        scoreRepository.save(score1);
                        scoreRepository.save(score2);
                        scoreRepository.save(score3);
                        scoreRepository.save(score4);
                        scoreRepository.save(score5);
                        scoreRepository.save(score6);
                        scoreRepository.save(score7);
                        scoreRepository.save(score8);


                };
        }

        //@Autowired
        //PasswordEncoder passwordEncoder;

        @Bean
        public PasswordEncoder passwordEncoder() {
                return PasswordEncoderFactories.createDelegatingPasswordEncoder();
        }

}





@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

        @Autowired
        PlayerRepository playerRepository;

        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {
                auth.userDetailsService(inputName -> {
                        Player player = playerRepository.findByUserName(inputName);
                        if (player != null) {
                                return new User(player.getUserName(), player.getPassword(),
                                        AuthorityUtils.createAuthorityList("USER"));
                        } else {
                                throw new UsernameNotFoundException("Unknown user: " + inputName);
                        }
                });




        }
}


@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {


                @Override
                protected void configure(HttpSecurity http) throws Exception {

                        http.authorizeRequests()
                                .antMatchers("/web/games_3.html").permitAll()
                                .antMatchers("/web/**").permitAll()
                                .antMatchers("/api/games").permitAll()
                                .antMatchers("/api/players").permitAll()
                                .antMatchers("/api/leaderBoard").permitAll()
                                .antMatchers("/api/game_view/*").hasAuthority("USER")
                                .antMatchers("/rest/*").denyAll()
                                .anyRequest().permitAll();



                        http.formLogin()
                                .usernameParameter("username")
                                .passwordParameter("password")
                                .loginPage("/api/login");

                        http.logout()
                                .logoutUrl("/api/logout");

                        // turn off checking for CSRF tokens
                        http.csrf().disable();

                        // if user is not authenticated, just send an authentication failure response
                        http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

                        // if login is successful, just clear the flags asking for authentication
                        http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

                        // if login fails, just send an authentication failure response
                        http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

                        // if logout is successful, just send a success response
                        http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());


                        http.authorizeRequests()
                                .anyRequest()
                                .fullyAuthenticated()
                                .and()
                                .httpBasic();
                }



        private void clearAuthenticationAttributes(HttpServletRequest request) {
                HttpSession session = request.getSession(false);
                if (session != null) {
                        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
                }
        }

}





