package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.List;
import java.util.Set;


@Entity
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private String userName;

    //private String email;


    private String password;

    @OneToMany(mappedBy = "player", fetch =FetchType.EAGER) //player es el nombre del atributo en GamePlayer
    private Set<GamePlayer> gamePlayers;

    @OneToMany(mappedBy = "player", fetch =FetchType.EAGER)
    private List<Score> scores;




    //constructores
    public Player(String userName, String password) {
        this.userName = userName;
        //this.email = email;
        this.password = password;
    }

    public Player() {
    }



    public Set<GamePlayer> getGamePlayers() {
        return gamePlayers;
    }

    public void setGamePlayer(Set<GamePlayer> gamePlayers) {
        this.gamePlayers = gamePlayers;
    }


    public long getWins(List<Score> scores){
        return scores
                .stream()
                .filter(score -> score.getScore() == 1.0)
                .count();
    }

    public long getLoses(List<Score> scores){
        return scores
                .stream()
                .filter(score -> score.getScore() == 0.0)
                .count();
    }

    public long getDraws(List<Score> scores){
        return scores
                .stream()
                .filter(score -> score.getScore() == 0.5)
                .count();
    }

    public double getScore(Player player){
        return getWins(player.getScores())*1 + getDraws(player.getScores())*0.5;
    }

    public void addGamePlayer(GamePlayer gamePlayer){
        gamePlayer.setGamePlayer(this);
        gamePlayer.add(gamePlayer);
    }

    public Score getScore(Game game){
        return game.getScores().stream().filter(score -> score.getPlayer().equals(this)).findFirst().orElse(null);
    }

    //getters y setters
    public String getUserName() {
        return userName;
    }

/*    public String getEmail() {
        return email;
    }*/

    public void setUserName(String userName) {
        this.userName = userName;
    }

/*
    public void setEmail(String email) {
        this.email = userName;
    }
*/


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Score> getScores() {
        return scores;
    }

    public void setScores(List<Score> scores) {
        this.scores = scores;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}